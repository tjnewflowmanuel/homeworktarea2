package com.example.homework2_fragmentsedilberto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.homework2_fragmentsedilberto.tabs.TabNavigationActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        next(TabNavigationActivity::class.java,null,true)

    }
}
